﻿namespace Custom.Delegates
{
    using System;

    public class ImprovedCustomEvent
    {
        public event EventHandler<ImprovedCustomEventArgs> ImprovedCustomEventHandler;

        public event EventHandler WorkCompleted;

        public void DoWork(int x, int y)
        {
            OnDoWork(x, y);
            OnWorkCompleted();
        }

        protected virtual void OnDoWork(int x, int y)
        {
            var del = ImprovedCustomEventHandler as EventHandler<ImprovedCustomEventArgs>;
            if (del != null)
            {
                del(this, new ImprovedCustomEventArgs { x = x, y = y });
            }
        }

        protected virtual void OnWorkCompleted()
        {
            var delCompleted = WorkCompleted as EventHandler;
            if (delCompleted != null)
            {
                delCompleted(this, EventArgs.Empty);
            }
        }
    }

    public class ImprovedCustomEventArgs : EventArgs
    {
        public int x { get; set; }
        public int y { get; set; }
    }

}
