﻿using System;

namespace Custom.Delegates
{
    class Program
    {
        delegate void DoWork(int a, int b);

        static DoWork addition = new DoWork(sum);

        static void sum(int x, int y)
        {
            var sum = x + y;
            Console.WriteLine("Sum : {0}", sum);
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Using Delegate");
            addition(4, 5);

            Console.WriteLine("Using CustomEvent");

            var eventExample = new CustomEvent();

            eventExample.DoWork(4, 5);

            Console.WriteLine("Using ImprovedCustomEvent");
            var improvedEvent = new ImprovedCustomEvent();
            improvedEvent.ImprovedCustomEventHandler += (x, e) =>
            {
                var sum = e.x + e.y;
                Console.WriteLine("Sum : {0}", sum);
            };

            improvedEvent.WorkCompleted += ImprovedEvent_WorkCompleted;
            improvedEvent.DoWork(22, 10);

            Console.ReadLine();
        }

        private static void ImprovedEvent_WorkCompleted(object sender, EventArgs e)
        {
            Console.WriteLine("Worker is completed");
        }
    }
}
