﻿using System;

namespace Custom.Delegates
{
    public class CustomEvent
    {
        public delegate void PerformWorkHandler(int x, int y);

        public event PerformWorkHandler WorkPerformed; //Event Definition

        public event EventHandler WorkCompleted; //Event Definition

        public CustomEvent()
        {
            WorkPerformed = Sum;
        }

        public void DoWork(int x, int y)
        {

            OnWorkPerformed(x, y);

            OnWorkCompleted();
        }

        protected virtual void OnWorkPerformed(int x, int y)
        {
            //var delegateWorkPerformed = WorkPerformed as PerformWorkHandler;
            //if(delegateWorkPerformed != null)
            //{
            //    delegateWorkPerformed(x, y);
            //}

            if (WorkPerformed != null)
            {
                WorkPerformed(x, y);
            }
        }

        protected virtual void OnWorkCompleted()
        {
            //if(WorkCompleted !=null)
            //{
            //    WorkCompleted(this, EventArgs.Empty);

            //}

            var delegateWorkCompleted = WorkCompleted as EventHandler;
            if (delegateWorkCompleted != null)
            {
                delegateWorkCompleted(this, EventArgs.Empty);
            }
        }

        private void Sum(int x, int y)
        {
            var sum = x + y;
            Console.WriteLine("Sum : {0}", sum);
        }
    }
}
